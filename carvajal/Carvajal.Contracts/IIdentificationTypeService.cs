﻿using Carvajal.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Carvajal.Contracts
{
    public interface IIdentificationTypeService
    {
        IdentificationType GetById(int IdentificationTypeId);
    }
}
