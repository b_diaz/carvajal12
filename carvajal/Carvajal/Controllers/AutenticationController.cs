﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Carvajal.Contracts;
using Carvajal.Models;
using Carvajal.ViewModels;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;

namespace Carvajal.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AutenticationController : ControllerBase
    {
        #region  ------------ Dependencias -------------
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly SignInManager<ApplicationUser> _signInManager;


        public AutenticationController(
            UserManager<ApplicationUser> userManager,
            SignInManager<ApplicationUser> signInManager
            )
        {
            _userManager = userManager;
            _signInManager = signInManager;
        }
        #endregion

        #region ---------- Login ------------
        /// <summary>
        /// Iniciar Sesion
        /// </summary>
        [HttpPost]
        public async Task<IActionResult> Login([FromBody] LoginViewModel model)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    var user = await _userManager.FindByEmailAsync(model.Email);

                    if (user == null)
                    {
                        return NotFound("El correo en relacion no se encuentra registrado en nuestro sistema");
                    }

                    var result = await _signInManager.PasswordSignInAsync(model.Email, model.Password, false, lockoutOnFailure: false);
                    if (result.Succeeded)
                    {
                        return Ok("Inicio de sesion realizado con exito");
                    }
                    else
                    {
                        return BadRequest("Contraseña Incorrecta");
                    }

                }
                catch (Exception ex)
                {
                    return StatusCode(500, new ObjectResult(ex));
                }
            }
            else
            {
                return BadRequest("Modelo no valido");
            }

        }
        #endregion
    }
}