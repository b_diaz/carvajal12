﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Carvajal.Contracts;
using Carvajal.Models;
using Carvajal.ViewModels;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace Carvajal.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {

        #region  ------------ Dependencias -------------
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly IIdentificationTypeService _IdentificationTypeService;
        private readonly ILogger _logger;


        public UserController(
            UserManager<ApplicationUser> userManager,
            IIdentificationTypeService IdentificationTypeService,
            ILogger<UserController> logger
            )
        {
            _userManager = userManager;
            _IdentificationTypeService = IdentificationTypeService;
            _logger = logger;
        }
        #endregion

        #region ---------------- READ --------------
        /// <summary>
        /// Consultar usuarios por email
        /// </summary>
        [HttpGet("{email}")]
        public async Task<IActionResult> Read(string email)
        {
            try
            {
                var user = await _userManager.FindByEmailAsync(email);

                if (user == null)
                {
                    _logger.LogInformation("Se consulta un correo que no existe "+ email);
                    return NotFound("El correo relacionado no se ha registrado en nuestro sistema");
                }

                return Ok(user);
            }
            catch (Exception ex)
            {
                _logger.LogInformation("Excepcion en Read " + ex.Message);
                return StatusCode(500, new ObjectResult(ex));
            }

        }
        #endregion

        #region --------------- CREATE --------------
        /// <summary>
        /// Crear Usuarios, los IdentificationTypeId disponibles son: 1, 2, 3, 4, 5, agregados en CarvajalDbContext
        /// </summary>
        [HttpPost]
        public async Task<IActionResult> Create([FromBody] ApplicationUserViewModel model)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    var user = await _userManager.FindByIdAsync(model.Email);

                    if (user != null)
                    {
                        return BadRequest("El correo asociado ya se encuentra registrado en nuestra base de datos");
                    }

                    if (this._IdentificationTypeService.GetById(model.IdentificationTypeId) == null)
                    {
                        return BadRequest("El IdentificationTypeId no es correcto debe ser un numero entre el 1 y 5");
                    }

                    var newUser = new ApplicationUser
                    {
                        Email = model.Email,
                        FirstName = model.FirstName,
                        LastName = model.LastName,
                        UserName = model.Email,
                        IdentificationTypeId = model.IdentificationTypeId,
                        IdentificationNumber = model.IdentificationNumber
                    };

                    var result = await _userManager.CreateAsync(newUser, model.Password);
                    if (result.Succeeded)
                    {
                        _logger.LogInformation("Se crea un nuevo usuario " + newUser.Email);
                        return Ok(newUser);
                    }
                    else
                    {
                        return BadRequest(result.Errors.First().Description.ToString());
                    }


                }
                catch (Exception ex)
                {
                    _logger.LogInformation("Excepcion en Create " + ex.Message);
                    return StatusCode(500, new ObjectResult(ex));
                }

            }
            else
            {
                return BadRequest("Modelo no valido");
            }

        }
        #endregion

        #region --------------- UPDATE --------------
        /// <summary>
        /// Actualizar Usuario, los IdentificationTypeId disponibles son: 1, 2, 3, 4, 5, agregados en CarvajalDbContext
        /// </summary>
        /// 
        [HttpPut]
        public async Task<IActionResult> Update([FromBody] ApplicationUserUpdateViewModel model)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    var user = await _userManager.FindByEmailAsync(model.Email);

                    if (user == null)
                    {
                        return NotFound("El correo relacionado no se ha registrado en nuestro sistema");
                    }

                    if (this._IdentificationTypeService.GetById(model.IdentificationTypeId) == null)
                    {
                        return BadRequest("El IdentificationTypeId no es correcto debe ser un numero entre el 1 y 5");
                    }

                    user.FirstName = model.FirstName;
                    user.LastName = model.LastName;
                    user.IdentificationTypeId = model.IdentificationTypeId;
                    user.IdentificationNumber = model.IdentificationNumber;

                    var result = await _userManager.UpdateAsync(user);
                    if (result.Succeeded)
                    {
                        return Ok(user);
                    }
                    else
                    {
                        return BadRequest(result.Errors.First().Description.ToString());
                    }
                }
                catch (Exception ex)
                {
                    _logger.LogInformation("Excepcion en Update " + ex.Message);
                    return StatusCode(500, new ObjectResult(ex));
                }
            }
            else
            {
                return BadRequest("Modelo no valido");
            }
        }

        #endregion

        #region ------------- DELETE ---------------
        /// <summary>
        /// Eliminar Usuario por email
        /// </summary>
        [HttpDelete("{email}")]
        public async Task<IActionResult> Delete(string email)
        {
            try
            {
                var user = await _userManager.FindByEmailAsync(email);

                if (user == null)
                {
                    return BadRequest("El correo relacionado no se ha registrado en nuestro sistema");
                }


                var result = await _userManager.DeleteAsync(user);

                if (result.Succeeded)
                {
                    _logger.LogInformation("Se Actualizado el usuario " + email);
                    return Ok("El usuario se ha eleiminado correctamente");
                }
                else
                {
                    return BadRequest(result.Errors.First().Description.ToString());
                }
            }
            catch (Exception ex)
            {
                _logger.LogInformation("Excepcion en Delete " + ex.Message);
                return StatusCode(500, new ObjectResult(ex));
            }
        }
        #endregion

       

    }
}


