﻿using Carvajal.DataAccess;
using Carvajal.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using Carvajal.Contracts;

namespace Carvajal.Services
{
    public class IdentificationTypeService: IIdentificationTypeService
    {
        #region --------- Dependencias ------------

        private readonly IRepository<IdentificationType, int> _IdentificationTypeRepository;

        public IdentificationTypeService
            (IRepository<IdentificationType, int> IdentificationTypeRepository)
        {
            this._IdentificationTypeRepository = IdentificationTypeRepository;
        }
        #endregion

        public IdentificationType GetById(int IdentificationTypeId)
        {
            try
            {
                var result = _IdentificationTypeRepository.Query()
               .Where(x => x.Id == IdentificationTypeId)
               .FirstOrDefault();

                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }


        }
    }
}
