﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Text;

namespace Carvajal.Models
{
    public class ApplicationUser: IdentityUser
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public int IdentificationTypeId { get; set; }
        public IdentificationType IdentificationType { get; set; }
        public int IdentificationNumber { get; set; }

    }
}
