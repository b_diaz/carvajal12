﻿using Carvajal.Models.Base;
using System;
using System.Collections.Generic;
using System.Text;

namespace Carvajal.Models
{
    public class IdentificationType : EntityBase<int>
    {
        public string Name { get; set; }
    }
}
